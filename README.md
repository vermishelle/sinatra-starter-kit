# Sinatra starter kit

Includes:

- SASS
- Foundation 6
- Gulp
- HAML
- Font Awesome
- JSON

## Prepare to run
`bundle install && npm install`

## How to run locally

`gulp`

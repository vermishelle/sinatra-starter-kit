require 'sinatra/base'
require 'tilt/haml'
require 'haml'
require "sinatra/json"

class StarterApp < Sinatra::Base

  get '/' do
    haml :home
  end

# чтоб и ruby app.rb работало, и passenger
# см. стр. 70 книги Sinatra Up and Running
run! if __FILE__ == $0

end

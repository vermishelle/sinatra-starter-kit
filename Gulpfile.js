'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
var shell = require('gulp-shell');

const input = './sass/**/*.scss';
const output = './public/css';


gulp.task('sass', function () {
  return gulp.src(input)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(output));
});

gulp.task('sass:watch', function () {
  gulp.watch(input, ['sass']);
});

gulp.task('default', ['sass', 'sass:watch', 'passenger']);

gulp.task('passenger', shell.task([
  'bundle exec passenger start'
]));
